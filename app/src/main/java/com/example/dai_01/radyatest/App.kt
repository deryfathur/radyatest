package com.example.dai_01.radyatest

import android.app.Application
import com.example.dai_01.radyatest.dagger.component.AppComponent
import com.example.dai_01.radyatest.dagger.component.DaggerAppComponent
import com.example.dai_01.radyatest.dagger.module.ApiModule
import com.example.dai_01.radyatest.dagger.module.AppModule
import com.example.dai_01.radyatest.dagger.module.NetworkModule

class App : Application() {

    companion object {
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        component = DaggerAppComponent.builder()
                    .appModule(AppModule(this))
                    .networkModule(NetworkModule("https://maps.googleapis.com"))
                    .apiModule(ApiModule())
                    .build()
    }
}