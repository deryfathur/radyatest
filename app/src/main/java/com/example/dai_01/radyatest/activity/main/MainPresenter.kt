package com.example.dai_01.radyatest.activity.main

import com.example.dai_01.radyatest.dagger.qualifier.Authorized
import com.example.dai_01.radyatest.extension.errorConverter
import com.example.dai_01.radyatest.mvp.Presenter
import com.example.dai_01.radyatest.service.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class MainPresenter @Inject constructor(
        @Authorized val api: ApiService,
        val retrofit: Retrofit
): Presenter<MainView>{

    private var view : MainView? = null

    var addressListDisposables = Disposables.empty()

    override fun onAttach(view: MainView) {
        this.view = view
    }

    override fun onDetach() {
        view = null
    }

    fun getAddressByLatLong(lat:String,long:String,key:String){

        addressListDisposables.dispose()
        var latLong = lat+","+long
        addressListDisposables = api.getAddressByLatLong(latLong,key)
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    res ->

                    view?.onLoadAddressSuccess(res)

                }, {
                    err ->
                    if (err is HttpException) {
                        val body = retrofit.errorConverter<Response<Throwable>>(err)
                        view?.onLoadAddressFailed("Error: ${body.message()}")
                    } else {
                        view?.onLoadAddressFailed(err.localizedMessage)
                    }
                })
    }


    fun getAddressBySearch(searchString:String,key:String){

        addressListDisposables.dispose()
        addressListDisposables = api.getAddressBySearch(searchString,key)
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    res ->

                    view?.onLoadAddressSearchSuccess(res)

                }, {
                    err ->
                    if (err is HttpException) {
                        val body = retrofit.errorConverter<Response<Throwable>>(err)
                        view?.onLoadAddressSearchFailed("Error: ${body.message()}")
                    } else {
                        view?.onLoadAddressSearchFailed(err.localizedMessage)
                    }
                })
    }


}