package com.example.dai_01.radyatest.activity.main

import com.example.dai_01.radyatest.model.AddressResponse
import com.example.dai_01.radyatest.model.SearchResponse
import com.example.dai_01.radyatest.mvp.View


interface MainView :View {

   fun onLoadAddressSuccess(message : AddressResponse)

   fun onLoadAddressFailed(message: String)

   fun onLoadAddressSearchSuccess(message : SearchResponse)

   fun onLoadAddressSearchFailed(message: String)

}