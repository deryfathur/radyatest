package com.example.dai_01.radyatest.model

data class SearchResponse(
    val candidates: List<Candidate>,
    val status: String
)

data class Candidate(
    val formatted_address: String
)

