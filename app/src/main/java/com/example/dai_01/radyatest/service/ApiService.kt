package com.example.dai_01.radyatest.service

import com.example.dai_01.radyatest.model.AddressResponse
import com.example.dai_01.radyatest.model.SearchResponse
import retrofit2.http.GET
import io.reactivex.Observable
import retrofit2.http.Query

interface ApiService {

    @GET("/maps/api/geocode/json?")
    fun getAddressByLatLong(@Query("latlng") latlng :String, @Query("key") key :String): Observable<AddressResponse>

    @GET("/maps/api/place/findplacefromtext/json?inputtype=textquery&fields=formatted_address")
    fun getAddressBySearch(@Query("input") input :String, @Query("key") key :String): Observable<SearchResponse>


}