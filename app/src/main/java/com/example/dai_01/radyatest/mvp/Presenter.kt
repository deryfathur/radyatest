package com.example.dai_01.radyatest.mvp

interface Presenter<in T: View> {

    fun onAttach(view: T)
    fun onDetach()

}