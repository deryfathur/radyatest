package com.example.dai_01.radyatest.activity.main.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.dai_01.radyatest.R
import com.example.dai_01.radyatest.extension.inflate
import com.example.dai_01.radyatest.model.Candidate
import kotlinx.android.synthetic.main.user_card.view.*

class RecyclerSearchListAdapter (private val items:List<Candidate>) : RecyclerView.Adapter<RecyclerSearchListAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerSearchListAdapter.ViewHolder {
        val inflatedView= parent!!.inflate(R.layout.item_search,false)
        return RecyclerSearchListAdapter.ViewHolder(inflatedView)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerSearchListAdapter.ViewHolder?, position: Int) {
        val view=holder?.itemView
        val data=items[position]

        view?.let {
            it.visibility=View.VISIBLE
            (it.findViewById(R.id.tv_search_item) as TextView).text=data.formatted_address

        }

        view?.setOnClickListener {

        }
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v){

    }
}