package com.example.dai_01.radyatest.activity.main

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.location.Location
import android.location.LocationListener
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.example.dai_01.radyatest.App
import com.example.dai_01.radyatest.R
import com.example.dai_01.radyatest.activity.main.adapter.AddressEvent
import com.example.dai_01.radyatest.activity.main.adapter.RecyclerSearchListAdapter
import com.example.dai_01.radyatest.extension.debug
import com.example.dai_01.radyatest.model.AddressResponse
import com.example.dai_01.radyatest.model.Candidate
import com.example.dai_01.radyatest.model.SearchResponse
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.trello.rxlifecycle2.LifecycleTransformer
import com.trello.rxlifecycle2.android.ActivityEvent
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_main.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.security.AccessController.getContext
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainView, OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    @Inject
    lateinit var presenter: MainPresenter
    private lateinit var vMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var searchAdapter:RecyclerSearchListAdapter

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>
    private lateinit var llBottomSheet: LinearLayout
    private lateinit var viewInfo : View

    private var curLoc = "Current Place"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        App.component.inject(this)
        EventBus.getDefault().register(this)
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        // get the bottom sheet view
        llBottomSheet = findViewById(R.id.bottom_sheet) as LinearLayout

        // init the bottom sheet behavior
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet)

        // change the state of the bottom sheet
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)


        onAttach()

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onAddressLoad(event: AddressEvent) {
        println("masuk event ${event.address}")
        viewInfo.findViewById<TextView>(R.id.tv_address_desc).text = event.address
        curLoc = event.address

    }

    override fun onInfoWindowClick(p0: Marker?) {
        if(bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED){
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
                }else{
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
                }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        vMap = googleMap

        vMap.setOnInfoWindowClickListener(this)


        vMap.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter{

            override fun getInfoContents(p0: Marker?): View? {


                return viewInfo
            }

            override fun getInfoWindow(p0: Marker?): View {
                viewInfo = getLayoutInflater().inflate(R.layout.custom_marker_maps, null);
                viewInfo.findViewById<TextView>(R.id.tv_address_desc).text = curLoc
                return viewInfo;
            }
        })

        getCurrentLocation()


        vMap.setOnMapClickListener {

            println("klik")
            vMap.clear()
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            val imm = this@MainActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.hideSoftInputFromWindow(this@MainActivity.getCurrentFocus()?.getWindowToken(), 0)

            val pickLoc = LatLng(it.latitude,it.longitude)
            placeMarkerOnMap(pickLoc)
            vMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pickLoc, 12f))
        }
        vMap.getUiSettings().isMyLocationButtonEnabled = false

    }

    private fun getCurrentLocation(){
        if (ActivityCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        vMap.isMyLocationEnabled = true

        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->

            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                placeMarkerOnMap(currentLatLng)
                vMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
            }
        }
    }



    private fun placeMarkerOnMap(location: LatLng) {
        val markerOptions = MarkerOptions().position(location).icon(BitmapDescriptorFactory.defaultMarker( BitmapDescriptorFactory.HUE_BLUE));
        presenter.getAddressByLatLong(location.latitude.toString(),location.longitude.toString(),getString(R.string.api_key))
        val mark = vMap.addMarker(markerOptions)
        mark.showInfoWindow()
    }

    override fun onLoadAddressSuccess(message: AddressResponse) {

        println("SUCCESS ADDRESS : $message")

        if(message.results.size>0){
            EventBus.getDefault().post(AddressEvent(message.results[0].formatted_address))
            viewInfo.findViewById<TextView>(R.id.tv_address_desc).text = message.results[0].formatted_address
            llBottomSheet.findViewById<TextView>(R.id.edit_address).text = message.results[0].formatted_address
            llBottomSheet.findViewById<TextView>(R.id.edit_detail).text = message.results[0].plus_code.compound_code
            llBottomSheet.findViewById<TextView>(R.id.edit_label).text = message.results[0].types[0]
        }else{
            Toast.makeText(this,message.status,Toast.LENGTH_SHORT).show()
        }

    }

    override fun onLoadAddressFailed(message: String) {
        println("FAILED ADDRESS : $message")
    }

    private fun initiateRecyclerSearchView(data:List<Candidate>) {
        rv_search_result.isNestedScrollingEnabled=false
        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        rv_search_result.layoutManager=linearLayoutManager
        searchAdapter= RecyclerSearchListAdapter(data)
        rv_search_result.adapter=searchAdapter

    }

    override fun onLoadAddressSearchSuccess(message: SearchResponse) {
        println("SEARCH SUCCESS : $message")

        if(message.candidates.size>0){
            initiateRecyclerSearchView(message.candidates)

        }else{
            Toast.makeText(this,message.status,Toast.LENGTH_SHORT).show()
        }
    }

    override fun onLoadAddressSearchFailed(message: String) {
        println("SEARCH FAILED : $message")

    }

    override fun onAttach() {

        presenter.onAttach(this)

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        btn_current_location.setOnClickListener {

            vMap.clear()
            getCurrentLocation()
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
        }

        bt_clear.setOnClickListener {
            et_search.setText("")
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(et_search, 1)
        }

        bt_clear.visibility = View.GONE
        et_search.addTextChangedListener(textWatcher)

        et_search.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchAction()
                hideKeyboard()
                return@OnEditorActionListener true
            }
            false
        })

    }

    private fun hideKeyboard() {
        val view = et_search
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun searchAction() {

        presenter.getAddressBySearch(et_search.text.toString(),getString(R.string.api_key))
    }

    inner class WrapContentLinearLayoutManager : LinearLayoutManager(this) {
        //... constructor
        override fun onLayoutChildren(recycler: RecyclerView.Recycler?, state: RecyclerView.State) {
            try {
                super.onLayoutChildren(recycler, state)
            } catch (e: IndexOutOfBoundsException) {
                Log.e("Error", "IndexOutOfBoundsException in RecyclerView happens")
            }

        }
    }

    internal var textWatcher: TextWatcher = object : TextWatcher {
        override fun onTextChanged(c: CharSequence, i: Int, i1: Int, i2: Int) {
            if (c.toString().trim { it <= ' ' }.length == 0) {
                bt_clear.visibility = View.GONE
            } else {
                bt_clear.visibility = View.VISIBLE
            }
        }

        override fun beforeTextChanged(c: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun afterTextChanged(editable: Editable) {}
    }

    override fun onDetach() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun lifecycle(): Observable<ActivityEvent> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <T> bindTolifeCycle(): LifecycleTransformer<T> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onBackPressed() {

        finish()
        super.onBackPressed()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
